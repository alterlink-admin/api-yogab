<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Validator;
use Auth;

class ProfileController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255|unique:profiles',
            'nip'      => 'required|numeric',
            'phone'    => 'required|numeric',
            'user_id'  => 'string|'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->errors()
            ]);
        }

        $profile = Profile::create([
            'fullname'  => $request->fullname,
            'nip'       => $request->nip,
            'phone'     => $request->phone,
            'user_id'   => Auth::id(),
            'position'   => $request->position
        ]);

        return response()->json([
            'status' => (bool)$profile
        ]);
    }

    public function show()
    {
        $user = Auth::user();
        return response()->json([
            'profile'   => $user->profile
        ]);
    }

    public function update(Request $request)
    {
        $user_id = Auth::id();
        $data = Profile::find($user_id);
        $data->fullname = $request['fullname'];
        $data->nip      = $request['nip'];
        $data->phone    = $request['phone'];
        $data->position = $request['position'];
        $data->save();

        return response()->json([
            'profile'   => $data
        ]);
    }
}
