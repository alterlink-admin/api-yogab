<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\UserResource;
use Validator;
use Auth;

class UserController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(function ($request, $next) {
    //         $this->projects = Auth::user()->profile;

    //         return $next($request);
    //     });
    // }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ]);
        }

        $status = 401;
            $response = ['error' => 'Unauthorised'];

            if (Auth::attempt($request->only(['email', 'password']))) {
                $status = 200;
                $user = Auth::user()->stands;
                $response = [
                    'user' => Auth::user(),
                    'token' => Auth::user()->createToken('yogab')->accessToken,
                ];
            }

            return response()->json($response, $status);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|unique:users|max:255',
            'email'         => 'required|email',
            'password'      => 'required|min:6|',
            'c_password'    => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ]);
        }

        $data = $request->only('email', 'name', 'password');
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);

        return response()->json([
            'user'  => $user
        ]);
    }

    public function me()
    {
        $user = Auth::user();
        return response()->json([
            'user'  => new UserResource($user)
        ]);
    }
}