<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'fullname',
        'nip',
        'phone',
        'birthday',
        'position'
    ];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Position()
    {
        return $this->belongTo(Position::class);
    }
}
